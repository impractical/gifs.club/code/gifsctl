module gifs.club/code/gifsctl

// TODO: replace this with an actual version
replace gifs.club/code/go-gifs/gifs v0.0.0 => ../go-gifs/gifs

require (
	darlinggo.co/version v1.0.0
	gifs.club/code/go-gifs/gifs v0.0.0
	github.com/mitchellh/cli v1.0.0
)
