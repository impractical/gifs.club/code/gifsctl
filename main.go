package main

import (
	"fmt"
	"net/http"
	"net/url"
	"os"

	"darlinggo.co/version"
	"gifs.club/code/go-gifs/gifs"
	"github.com/mitchellh/cli"
)

// TODO(paddy): CLI for gifsd

func main() {
	c := cli.NewCLI("gifsctl", fmt.Sprintf("%s (%s)", version.Tag, version.Hash))
	c.Args = os.Args[1:]
	ui := &cli.ColoredUi{
		InfoColor:  cli.UiColorCyan,
		ErrorColor: cli.UiColorRed,
		WarnColor:  cli.UiColorYellow,
		Ui: &cli.BasicUi{
			Reader:      os.Stdin,
			Writer:      os.Stdout,
			ErrorWriter: os.Stderr,
		},
	}
	u, err := url.Parse("http://localhost:12345/")
	if err != nil {
		ui.Error("error parsing URL: " + err.Error())
		os.Exit(1)
	}
	client := gifs.NewClient(http.DefaultClient, u)
	c.Commands = map[string]cli.CommandFactory{
		// upload a gif from the command line
		"gifs upload": gifUploadCommandFactory(client, ui),
		"gifs up":     gifUploadCommandFactory(client, ui),
		"upload":      gifUploadCommandFactory(client, ui),

		// download a gif to your hard drive
		"gifs download": gifDownloadCommandFactory(ui),
		"gifs down":     gifDownloadCommandFactory(ui),
		"download":      gifDownloadCommandFactory(ui),

		// get info about a gif
		"gifs describe": gifDescribeCommandFactory(ui),
		"gifs info":     gifDescribeCommandFactory(ui),

		// update info about a gif
		"gifs set":    gifSetCommandFactory(ui),
		"gifs update": gifSetCommandFactory(ui),

		// remove a gif
		"gifs rm":     gifRmCommandFactory(ui),
		"gifs remove": gifRmCommandFactory(ui),
		"gifs delete": gifRmCommandFactory(ui),

		// get a list of gifs
		"gifs list": gifListCommandFactory(ui),
		"gifs ls":   gifListCommandFactory(ui),

		// create a collection
		"collections start":  collectionStartCommandFactory(client, ui),
		"collections new":    collectionStartCommandFactory(client, ui),
		"collections create": collectionStartCommandFactory(client, ui),

		// update a collection
		"collections set":    collectionSetCommandFactory,
		"collections update": collectionSetCommandFactory,

		// remove a collection
		"collections rm":     collectionRmCommandFactory,
		"collections remove": collectionRmCommandFactory,
		"collections delete": collectionRmCommandFactory,

		// get info about a collection
		"collections describe": collectionDescribeCommandFactory,
		"collections info":     collectionDescribeCommandFactory,
		"collections get":      collectionDescribeCommandFactory,

		// list collections
		"collections list": collectionListCommandFactory,

		// sign in
		"auth login": authLoginCommandFactory(ui),
		"login":      authLoginCommandFactory(ui),

		// sign out
		"auth logout": authLogoutCommandFactory(ui),
		"logout":      authLogoutCommandFactory(ui),

		// get auth info
		"auth whoami": authWhoamiCommandFactory(ui),
		"auth info":   authWhoamiCommandFactory(ui),
		"whoami":      authWhoamiCommandFactory(ui),

		// check for updates
		"version": versionCommandFactory(ui),
	}

	c.HiddenCommands = []string{
		"download",
		"upload",
		"gifs down",
		"gifs up",
		"gifs info",
		"gifs update",
		"gifs remove",
		"gifs delete",
		"gifs ls",
		"collections new",
		"collections create",
		"collections update",
		"collections remove",
		"collections delete",
		"collections info",
		"collections get",
		"login",
		"logout",
		"auth info",
		"whoami",
	}

	c.Autocomplete = true

	status, err := c.Run()
	if err != nil {
		fmt.Println(err)
	}
	os.Exit(status)
}
