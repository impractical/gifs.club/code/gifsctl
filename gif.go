package main

import (
	"context"
	"fmt"
	"net/url"
	"os"
	"strings"

	"gifs.club/code/go-gifs/gifs"
	"github.com/mitchellh/cli"
)

func gifUploadCommandFactory(client *gifs.Client, ui cli.Ui) func() (cli.Command, error) {
	return func() (cli.Command, error) {
		return &gifUploadCommand{
			UI:     ui,
			client: client,
		}, nil
	}
}

type gifUploadCommand struct {
	UI     cli.Ui
	client *gifs.Client
}

func (g gifUploadCommand) Help() string {
	return "some very informative help text"
}

func (g gifUploadCommand) Run(args []string) int {
	if len(args) < 1 {
		g.UI.Error("must specify file to upload")
		return 1
	}
	if len(args) < 2 {
		g.UI.Error("must specify target URL for file")
		return 1
	}
	path := args[1]
	if !strings.Contains(path, "://") {
		path = "https://" + path
	}
	u, err := url.Parse(path)
	if err != nil {
		g.UI.Error(fmt.Sprintf("%q is not a valid URL", path))
		return 1
	}
	slug := u.Hostname()
	collection, err := g.client.Collections().GetByDomain(context.Background(), slug)
	if err != nil {
		g.UI.Error("Error finding collection \"" + slug + "\": " + err.Error())
		return 1
	}
	path = u.Path
	f, err := os.Open(args[0])
	if err != nil {
		g.UI.Error("error opening file: " + err.Error())
		return 1
	}
	// TODO: use github.com/cheggaaa/pb to display progress
	gif, err := g.client.GIFs().Upload(context.Background(), collection.ID, path, f)
	if err != nil {
		g.UI.Error("error uploading file: " + err.Error())
		return 1
	}
	g.UI.Info(fmt.Sprintf("Successfully uploaded GIF: %+v", gif))
	return 0
}

func (g gifUploadCommand) Synopsis() string {
	return "Upload a GIF to a collection."
}

func gifDownloadCommandFactory(ui cli.Ui) func() (cli.Command, error) {
	return func() (cli.Command, error) {
		return &gifDownloadCommand{
			UI: &cli.ColoredUi{
				Ui: ui,
			},
		}, nil
	}
}

type gifDownloadCommand struct {
	UI cli.Ui
}

func (g gifDownloadCommand) Help() string {
	return "some very informative help text"
}

func (g gifDownloadCommand) Run(args []string) int {
	fmt.Printf("uploading %q, I guess?\n", args[0])
	return 0
}

func (g gifDownloadCommand) Synopsis() string {
	return "Download a GIF to your hard drive."
}

func gifDescribeCommandFactory(ui cli.Ui) func() (cli.Command, error) {
	return func() (cli.Command, error) {
		return &gifDescribeCommand{
			UI: &cli.ColoredUi{
				Ui: ui,
			},
		}, nil
	}
}

type gifDescribeCommand struct {
	UI cli.Ui
}

func (g gifDescribeCommand) Help() string {
	return "some very informative help text"
}

func (g gifDescribeCommand) Run(args []string) int {
	fmt.Printf("uploading %q, I guess?\n", args[0])
	return 0
}

func (g gifDescribeCommand) Synopsis() string {
	return "Display information about a GIF."
}

func gifSetCommandFactory(ui cli.Ui) func() (cli.Command, error) {
	return func() (cli.Command, error) {
		return &gifSetCommand{
			UI: &cli.ColoredUi{
				Ui: ui,
			},
		}, nil
	}
}

type gifSetCommand struct {
	UI cli.Ui
}

func (g gifSetCommand) Help() string {
	return "some very informative help text"
}

func (g gifSetCommand) Run(args []string) int {
	fmt.Printf("uploading %q, I guess?\n", args[0])
	return 0
}

func (g gifSetCommand) Synopsis() string {
	return "Modify a GIF."
}

func gifRmCommandFactory(ui cli.Ui) func() (cli.Command, error) {
	return func() (cli.Command, error) {
		return gifRmCommand{
			UI: ui,
		}, nil
	}
}

type gifRmCommand struct {
	UI cli.Ui
}

func (g gifRmCommand) Help() string {
	return "some very informative help text"
}

func (g gifRmCommand) Run(args []string) int {
	fmt.Printf("uploading %q, I guess?\n", args[0])
	return 0
}

func (g gifRmCommand) Synopsis() string {
	return "Remove a GIF."
}

func gifListCommandFactory(ui cli.Ui) func() (cli.Command, error) {
	return func() (cli.Command, error) {
		return gifListCommand{
			UI: ui,
		}, nil
	}
}

type gifListCommand struct {
	UI cli.Ui
}

func (g gifListCommand) Help() string {
	return "some very informative help text"
}

func (g gifListCommand) Run(args []string) int {
	fmt.Printf("uploading %q, I guess?\n", args[0])
	return 0
}

func (g gifListCommand) Synopsis() string {
	return "List GIFs in a collection."
}
